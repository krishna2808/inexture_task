import re

from datetime import date



def valid_password():
    password = input("Enter Password : ")

    password_combination = "^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$"
    result = re.findall(password_combination , password)
    if (result):
        return password
    else:
        print("Invalid password\nPassword Must contain Lowercase , Uppercase, number, Minimum 8 characters")
        return valid_password()

def valid_username():
    username = input("Enter Username : ")
    username_combination = "^[A-Za-z]\\w{5,29}$"  
    result = re.findall(username_combination, username)
    if(result and len(username) < 20):
        return username

    print("Invalid Username, username is look like hello123 or helloo and Minimum 6 characters ")
    return valid_username() 

def valid_name(content):
    name = input(f"{content}").upper()
    name_combination = "^[\-'a-zA-Z ]+$"
    result  = re.findall(name_combination, name)
    if(result and len(name) < 20):
        return name
    print("Please enter valid name ")
    return valid_name(content)    

def valid_mobile_number():
    mobile_number, is_valid  = valid_input_user("\nEnter Mobile Number : ")
    if(is_valid and len(mobile_number) == 10):
        return mobile_number
    print("Enter valid mobile number ")    
    return valid_mobile_number()    

def valid_address():
    address = input("Enter Home town address : ")
    address_combination = r"^(?=.*[0-9])(?=.*[a-z|A-Z])[a-zA-Z0-9 ]*$"

    result =  re.match(address_combination, address)
    if(result and len(address) < 20):
        return address
    print("Enter Valid Address First")
    return valid_address()

def take_decision(content):
    is_continue = input(f"{content }") 
    if(is_continue == "YES" or is_continue == 'yes' or is_continue == 'Yes'):
        return True
    elif(is_continue == 'NO' or is_continue == 'No' or is_continue == 'no'):
         return False
    else:
      return take_decision(content)           
           

    



def string_input(content):
    user_input = input(f"{content}").upper()

    if(user_input == '' or user_input.isspace() or len(user_input) > 16):
        print("\nEnter valid input ")
        return string_input(content)
    return user_input, True

def valid_date_month_year(content):
    print(f"------------------------------\n{content}")
    try:
        year = input("\nEnter Year : ")
        month = input("\nEnter Month : ")
        date1 = input("\nEnter Date : ")
        time, is_correct = valid_input_user("\nEnter time 24 hour format  : ")
        if(int(time) >= 1 and int(time) <= 24):
            date_time = "{0}-{1}-{2} {3}:{4}:".format(date1, month, year, time, "00:00")    
            dates = date(int(year), int(month), int(date1))
            return date_time , dates 
        else:
            print("\nWrong Time ")    
    except ValueError as error:
        print(error)
    return valid_date_month_year(content)

  
 


def valid_input_user(content):

    user_input = input(f"{content}  ")

    if(user_input.isnumeric() ):
        return user_input, True
    else:
        print("\n {0:*^50s}  \n ".format(" Please Enter valid Input  "))
    
        return valid_input_user(content)

# valid_password()
# valid_username()
# content = 'Roll Number : '
# valid_input_user(content)

# valid_address()
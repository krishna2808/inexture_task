temp.py
from datetime import date
import sys
sys.path.append("..")
from Database.database_connection import DatabaseConnection

class DashBoard:

    def __init__(self, username):
        self.username = username
        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()


    def valid_input(self, user_input):
       if(user_input.isnumeric()):
       	   return True
       else:
            print("\n {0:*^50s}  \n ".format(" Please Enter valid Input  "))
            return False


    def showHistory(self):
        pass 
    def get_userId(self):
       sql = "SELECT * FROM users WHERE username = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(sql, (self.username, ))
       records = cursor.fetchall()
       # print(records)
       userId = records[0]
       print(userId)
       return userId


    def wallet(self):
        if(self.records != None):
            getAmount =  self.records[4]
            print("Your Wallet Amount : ", getAmount)
            choice  = input("\n[1] Add Amount\t [2] Back\nEnter Choice : ")
            if(self.valid_input(choice)):
                choice = int(choice)
                if(choice == 1):
                    # self.addAmount()
                    amount =  input("\nAdd Amount in Wallet : ")
                    if(self.valid_input(amount)):
                        user_obj = Users()
                        isAddAmount =  user_obj.addAmount(self.username, int(amount) + getAmount)
                        if(isAddAmount):
                            print("\nAmount is Successfull Add ")
                            self.show_user_interface()
                elif(choice == 2):
                     self.show_user_interface()
                else:
                    print("\nWrong choice")
                    self.show_user_interface()




    def menu(self):
        choice_sub_menu = input("\n[1] Show History\t [2] Wallet \t[3] Back\nEnter Choice : ")
        if(self.valid_input(choice_sub_menu)):
            choice_sub_menu = int(choice_sub_menu)
            if(choice_sub_menu == 1):
                self.showHistory()
            elif(choice_sub_menu == 2):
                self.wallet()
            elif(choice_sub_menu == 3):
                self.show_main_interface()    
            else:
                print("\nWrong Number ")
                self.show_main_interface()
    def pick_up_car(self):
       current_state = input("\nEnter current state name :  ").upper()
       current_city = input("\nEnter current city name : ").upper()
       cursor = self.isConnected.cursor()
       
       sql = "UPDATE users SET state = %s WHERE username = %s"
       cursor.execute(sql, (current_state, self.username ))
       self.isConnected.commit()
       cursor = self.isConnected.cursor()
       sql = "UPDATE users SET city = %s WHERE username = %s"
       cursor.execute(sql, (current_city, self.username))
       self.isConnected.commit()
       print("\nLocation Updated")
       sql = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(sql, (current_state, current_city, "YES"))
       records = cursor.fetchall()
       print(records)
       car_id_list, car_type_id_list, choice_car_id  = [], [], []
       if(len(records) != 0):
            for row in records:
                car_id_list.append(row[4])
                car_type_id_list.append(row[5])
                choice_car_id.append(row[4])
            # print(car_id_list)
            # print(car_type_id_list)   
            # print("--------------------------------") 
            for car_type_id in car_type_id_list:
                sql = "SELECT * FROM car_type WHERE car_type_id = %s "
                cursor = self.isConnected.cursor()
                cursor.execute(sql, (car_type_id, ))
                records = cursor.fetchall()
                if(len(records) != 0 ):
                    for row in records:
                        print("--------------------------------") 
                        print("\nCar Type Name : ", row[1])
                        car_id = car_id_list[0]
                        car_id_list.pop(0)
                        sql = "SELECT * FROM car_name WHERE car_id = %s"
                        cursor = self.isConnected.cursor()
                        cursor.execute(sql, (car_id, ))
                        records = cursor.fetchall()
                        if(len(records) !=0):

                            for row in records:
                               print("Car ID : ", car_id)
                               print("Car Name : ", row[1])
                               four_hour_price = row[3]
                               per_day_price = row[4]
                               print("Four Hour Price : ",four_hour_price)
                               print("Per Day Price : ", per_day_price)

            select_car_id  = input("\nEnter Car ID : ")
            print(select_car_id,  choice_car_id)
            if(int(select_car_id) in choice_car_id ):
                per_day_driver_price = 1000
                print("\nEnter Year and Month and Date  for pick Up car ")
                pick_year = input("\nEnter Year : ")
                pick_month = input("\nEnter Month : ")
                pick_date = input("\nEnter Date : ")
                pick_time = input("\nEnter time 24 hour format  : ")
                date1 = date(int(pick_year), int(pick_month), int(pick_date))
                car_pick_date = "{0}/{1}/{2}-{3}:{4}".format(pick_date, pick_month, pick_year, pick_time, "00:00")


                # choice_time = input("[1] Hour [2] Days \nChoice : ")
                # if(choice_time == '1'):
                    
                #     choice_hours = input("[1] 4 Hours [2] 8 Hours [3] 12 Hour \nChoice : ")


                #     if(choice_hours == '1'):
                #         price = four_hour_price

                #     elif(choice_hours == '2'):
                #         price = four_hour_price * 2 
                #     elif(choice_hours == '3'):
                #         price  = four_hour_price * 3 
                #     else:
                #         print("\nWrong choice time")
                # elif(choice_time == '2'):
                    
                print('\n ---------------------------------------------  ')
                print("\nEnter Year and Month and Date  for Drop Off car ")
                drop_year = input("\nEnter Year : ")
                drop_month = input("\nEnter Month : ")
                drop_date = input("\nEnter Date : ")
                drop_time = input("\nEnter time 24 hour format  : ")
                date2 = date(int(drop_year), int(drop_month), int(drop_date))
                car_drop_off_date = "{0}/{1}/{2}-{3}:{4}".format(drop_date, drop_month, drop_year, drop_time, "00:00")
                different = (date2 - date1).days
                if(different > 0):
                    price = different * per_day_price

                    is_want_driver = input("\nDo you want to driver yes/no ?: ")
                    payment_succesfully, is_driver_book = False, "NO" 
                    if(is_want_driver == 'YES' or is_want_driver == 'yes'):
                        total_driver_amount = per_day_driver_price * different
                        print("Driver's amount is : ", total_driver_amount)
                        total_amount = (per_day_price * different) + total_driver_amount
                        is_pay =  input("Do you want to Pay yes/no ? ")
                        if(is_pay == 'yes' or is_pay  == 'YES'):
                            sql = "SELECT * FROM users WHERE username = %s"
                            cursor = self.isConnected.cursor()
                            cursor.execute(sql, (self.username, ))
                            records = cursor.fetchall()
                            wallet =  records[5]
                            wallet = int(wallet)
                            if(  total_amount <= wallet):
                                  sql = "UPDATE users  SET wallet = %s WHERE username = %s"
                                  cur = self.isConnected.cursor()
                                  cur.execute(sql, (wallet - total_amount, self.username))
                                  price = total_amount
                                  self.isConnected.commit()
                                  is_driver_book = "YES"
                                  payment_succesfully = True


                        elif(is_pay == 'NO' or is_pay == 'no'  or is_pay == 'No'):
                                sql = "SELECT * FROM users WHERE username = %s"
                                cursor = self.isConnected.cursor()
                                cursor.execute(sql, (self.username, ))
                                records = cursor.fetchall()
                                wallet =  records[5]
                                wallet = int(wallet)
                                if(  total_amount <= wallet):
                                      sql = "UPDATE users  SET wallet = %s WHERE username = %s"
                                      cur = self.isConnected.cursor()
                                      cur.execute(sql, (wallet - price, self.username))
                                      self.isConnected.commit()
                                      payment_succesfully = True

                        else:
                                print("\nWrong Input ")    

                        if(payment_succesfully):
                           sql = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s and car_id = %s "
                           cursor = self.isConnected.cursor()
                           cursor.execute(sql, (current_state, current_city, "YES", select_car_id))
                           records = cursor.fetchall()
                           print(records)
                           car_number = records[0]

                           sql = "UPDATE car_details  SET is_car_available = %s WHERE car_number = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(sql, ("NO", car_number ))
                           self.isConnected.commit()
                           userId = self.get_userId()

                           sql = "INSERT INTO car_rent (car_number, price,  user_id, is_driver_booked, car_pick_up_date, car_drop_off_date) VALUES (%s, %s, %s, %s, %s, %s)"
                           cursor = self.isConnected.cursor()
                           details =  (car_number, price,  userId, is_driver_book, car_pick_date, car_drop_off_date)
                           cursor.execute(sql, details)
                           self.isConnected.commit()
                           print("\nCar rent Successfully")
                        

                else:
                     print("\nInvalid Date ")    





            
            else:
                print("\nInvalid select car id ")    

                    # choice_time = input("[1] Hour [2] Days \nChoice : ")
                    # if(choice_time == '1'):
                    #     choice_hours = input("[1] 4 Hours [2] 8 Hours [3] 12 Hour \nChoice : ")
                    #     if(choice_hours == '1'):
                    #         price = four_hour_price
                    #     elif(choice_hours == '2'):
                    #         price = four_hour_price * 2 
                    #     elif(choice_hours == '3'):
                    #         prince = four_hour_price * 3 
                    #     else:
                    #         print("\nWrong choice time")
                    # elif(choice_time == '2'):
                                


                    #     print("\nEnter Year and Month and Date  for pick Up car ")
                    #     pick_year = input("\nEnter Year : ")
                    #     pick_month = input("\nEnter Month : ")
                    #     pick_date = input("\nEnter Date : ")
                    #     pick_time = input("\nEnter time 24 hour format  : ")
                    #     date1 = date(pick_year, pick_month, pick_date)
                        
                    #     print('\n ---------------------------------------------  ')

                    #     print("\nEnter Year and Month and Date  for Drop Off car ")
                    #     drop_year = input("\nEnter Year : ")
                    #     drop_month = input("\nEnter Month : ")
                    #     drop_date = input("\nEnter Date : ")
                    #     drop_time = input("\nEnter time 24 hour format  : ")
                    #     date2 = date(drop_year, drop_month, drop_date)
                    #     if((date2 - date1).days > 0):




                    












           


       	           
            # print(car_id_list)
            # print(car_type_id_list)	    	
       else:
           print("\nCar is not available for current city ")
           return 	    	

       # print("\nEnter Year and Month and Date  for pick Up car ")
       # year = input("\nEnter Year : ")
       # month = input("\nEnter Month : ")
       # date = input("\nEnter Date : ")
       # time = input("\nEnter time 24 hour format  : ")









    def dash_board(self):
        choice_interface = input("\n[1] Menu\t [2] Pick Up Car \t[3]Log Out \nEnter Choice : ")
        if(self.valid_input(choice_interface)): 
            choice_interface = int(choice_interface)
            if(choice_interface == 1):
                self.menu()
            elif(choice_interface == 2 ):
                self.pick_up_car()
            elif(choice_interface == 3):
                print("\nSuccessfull LogOut Account \n")   
            else:
               print("\nWrong choice ")       


if __name__ == '__main__':
    dashboard = DashBoard('hello1')
    dashboard.dash_board()                     


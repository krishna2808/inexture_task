import sys
sys.path.append("..")
from Database.users import Users
from Database.database_connection import DatabaseConnection
from Admin.admin_dashboard import AdminDashBoard 
from Users.user_dashboard import UserDashBoard
from query import username_select


class SuccessfulLogin:
    def __init__(self, username):
        self.username = username

        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()
        self.records =  None 


    def show_admin_interface(self):
        dashboard = AdminDashBoard(self.username)
        dashboard.dash_board()


    def show_user_interface(self):
        
        dashboard = UserDashBoard(self.username)
        dashboard.dash_board()



    def isAdmin(self):
        if(self.isConnected != None):
            # sql = "select * from users where username = %s"
            cursor = self.isConnected.cursor()
            cursor.execute(username_select, (self.username, ))
            # it is return rows of data in list formate. 
            records = cursor.fetchall()

            # print("*****************888", records)
            
            for row in records:
                if(row[1] == self.username): 
                    self.records = [row[0], row[1], row[2], row[3], row[4]]
                    is_super_user = row[4]
                    break 
            # print("is_super_user ", is_super_user)        

            if(is_super_user == 'YES'):
                self.show_admin_interface()
            else:
                self.show_user_interface()    
                




if __name__ == "__main__":
    pass 
    